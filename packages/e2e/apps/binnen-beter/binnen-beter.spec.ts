import { expect } from '@playwright/test';

import { test } from '../../fixtures/test/index.js';

const appPath = 'binnen-beter';

test.describe('Binnen Beter', () => {
  test.beforeEach(async ({ loginApp, page, visitApp }) => {
    await visitApp(appPath);
    await page.getByRole('link', { name: 'Login' }).click();
    await loginApp('Tenant');
    await page.waitForLoadState('domcontentloaded');
    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
    await page
      .getByRole('heading', { name: 'Hi Appsemble Bot! Welcome to' })
      .waitFor({ state: 'visible' });
  });

  test.skip('should ...', ({}) => {
    expect(1 + 1).toBe(2);
  });
});
